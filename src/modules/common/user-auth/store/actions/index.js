import { CALL_API } from '../../../../../setup/config/env'
import * as types from '../constants/ActionTypes'

/**
 * Logout
 */
export const logout = () => (dispatch) => {
  clearLocalStorageData();
}

/**
 * Set user token and info in localStorage
 * @param {*} token : user token
 * @param {*} user : user data
 */
export function setLocalStorageData(token, user) {
  // Update token
  window.localStorage.setItem('token', token)
  window.localStorage.setItem('user', JSON.stringify(user))
}

/**
 * Clear localStorage
 */
export function clearLocalStorageData() {
  // Remove token
  window.localStorage.removeItem('token')
  window.localStorage.removeItem('user')
}


export const login = (payload) =>
  ({
    type: CALL_API,
    payload,
    meta: {
      actions: {
        init: types.LOGIN_ACCOUNT_INIT,
        success: types.LOGIN_ACCOUNT_SUCCEDED,
        fail: types.LOGIN_ACCOUNT_FAILED
      },
      endpoint: types.ENDPOINT_LOGIN,
      method: types.HTTP_METHODS_POST
    }
  })
