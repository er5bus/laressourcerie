import React, { Component } from 'react'
import { connect } from 'react-redux'
import { login } from './store/actions/index'
import { Button, Form, Modal } from 'semantic-ui-react'

class Login extends Component {

  constructor(props) {
    super(props);
    this.state = {
      email: null,
      password: null,
      modalOpen: true
    }
  }

  onSubmit = (e) => {
    e.preventDefault()
    const { email, password } = this.props
    this.props.login({ email, password })
  }

  handleOpen = () => this.setState({ modalOpen: true })

  handleClose = () => this.setState({ modalOpen: false })

  render() {
    return (
      <Modal open={this.state.modalOpen} onClose={this.handleClose}>
        <Modal.Header>Login</Modal.Header>
        <Modal.Content>
          <Form onSubmit={this.onSubmit}>
            <Form.Field>
              <label>Email</label>
              <input onChange={ e => this.setState({ email: e.target.value }) } name="email" type="text" placeholder="Email" />
            </Form.Field>
            <Form.Field>
              <label>Password</label>
              <input onChange={ e => this.setState({ password: e.target.value }) } name="password" type="password" placeholder="Password" />
            </Form.Field>
            <Button type='submit'>Submit</Button>
          </Form>
        </Modal.Content>
      </Modal>
    )
  }
}

// Get data from reducers
const mapStateToProps = (state) => state.authState;

export default connect(mapStateToProps, { login })(Login);
