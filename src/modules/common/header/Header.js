// Imports
import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'

// UI Imports TO - DO

// App Imports
import admin from '../../../setup/routes/admin'
import user from '../../../setup/routes/user'
import userAuth from '../../../setup/routes/user-auth'
import Menu from './Menu'
import MenuItem from './MenuItem'

// Component
const Header = (props) => {
    return (
        <header style={{
            padding: '0 2em',
            height: '5em'
        }}>
            <div style={{ marginTop: '1.5em' }}>
                <div>
                    {/* Left menu */}
                    <Menu style={{ float: 'left', marginTop: '0.5em', marginLeft: '2em' }}>
                        <MenuItem to={user.programs.path}>Programs</MenuItem>
                        <MenuItem to={user.ressourcerie.path}>Ressourcerie</MenuItem>
                    </Menu>
                </div>

                {/* Right menu */}
                <div style={{ textAlign: 'right' }}>
                    {
                        props.user.isAuthenticated
                            ?
                            <Menu>
                                {props.user.details.role === 'ADMIN' && <MenuItem to={admin.programs.path} section="admin">Admin</MenuItem>}
                                <MenuItem to={admin.programs.path}>Programs</MenuItem>
                            </Menu>
                            :
                            <Menu>
                                <MenuItem to={userAuth.login.path}>Login</MenuItem>

                                <MenuItem to={userAuth.signup.path}>Signup</MenuItem>
                            </Menu>
                    }
                </div>
            </div>
        </header>
    )
}

// Component Properties
Header.propTypes = {
    user: PropTypes.object.isRequired
}

// Component State
function headerState(state) {
    return {
        user: state.authState
    }
}

export default withRouter(connect(headerState, {})(Header))
