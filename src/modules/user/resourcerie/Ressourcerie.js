import React, { Component } from 'react'
import { connect } from 'react-redux'

import { getAllCustomers } from './store/actions'
import { getCustomers } from './store/reducers/customers'

import Button from "./../../../ui/button"

class Ressourcerie extends Component {

  render() {
    return (
      <div>
        Hellow Ressourcerie Container
        <br />
        <Button />
      </div>
    )
  }
}

// Get data from reducers
const mapStateToProps = (state) => ({
  customers: getCustomers(state.customers)
})

export default connect(mapStateToProps, { getAllCustomers })(Ressourcerie)
