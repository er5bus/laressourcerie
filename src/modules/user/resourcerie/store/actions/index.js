import * as types from '../constants/ActionTypes'

/**
 * Receive customers
 * @param  {} customers
 */
export const receiveCustomers = (customers) => ({
  type: types.RECEIVE_CUSTOMERS,
  customers
});

/**
 * Get all customers
 */
export const getAllCustomers = () => ({})
