// Imports
import React from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'

// App Imports
import { store } from './setup/store'
//import { setUserData, setLocalStorageData } from './modules/common/user-auth/store/actions'
//import { messageShow } from './modules/common/store/actions'

import { routes } from './setup/routes'
import Layout from './modules/common/Layout'
import NotFound from './modules/common/NotFound'
import RoutePrivate from './modules/auth/RoutePrivate'

import 'semantic-ui-css/semantic.min.css'


/* User Authentication
const token = window.localStorage.getItem('token')
if (token && token !== 'undefined' && token !== '') {
  const user = JSON.parse(window.localStorage.getItem('user'))
  if (user) {
    // Dispatch action
    store.dispatch(setUserData(token, user))

    setLocalStorageData(token, user)
  }
}
*/

render(
  <Provider store={store}>
    <Router>
      <Layout>
        <Switch>
          {Object.values(routes).map((route, index) => (
            route.auth
              ? <RoutePrivate {...route} key={index} path={typeof route.path === 'function' ? route.path() : route.path} />
              : <Route exact {...route} key={index} path={typeof route.path === 'function' ? route.path() : route.path} />
          ))}
          <Route component={NotFound} />
        </Switch>
      </Layout>
    </Router>
  </Provider>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
// serviceWorker.unregister();
