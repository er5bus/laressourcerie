import { combineReducers } from 'redux';
import commonMessage from '../../../modules/common/store/reducers/common-message'
import customers from '../../../modules/user/resourcerie/store/reducers/customers'
import authState from '../../../modules/common/user-auth/store/reducers/user-auth'

const appReducer = combineReducers({
    commonMessage,
    authState,
    customers
});

export default appReducer;
