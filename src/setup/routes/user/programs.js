// App Imports
import Programs from '../../../modules/user/programs/Programs'

// User Programs routes
export const programs = {
    path: '/programs',
    component: Programs,
    auth: false
}